import {Observable, Subject} from 'rxjs';
import {Article} from '../models/article';
import {ArticleService} from '../services/article.service';
import {tap} from 'rxjs/operators';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ArticleState {

  constructor(private articleService: ArticleService) {
  }

  private articleItemGet: Subject<Article[]> = new Subject<Article[]>();
  public articleItemGet$: Observable<Article[]> = this.articleItemGet.asObservable();


  public articleGetAll(): Promise<Article[]> {
    return this.articleService.getAllArticles().pipe(
      tap(data => {
        this.articleItemGet.next(data);
      })
    ).toPromise();
  }
}
