import {Component} from '@angular/core';
import {Article} from '../models/article';
import {ArticleService} from '../services/article.service';
import {ArticleState} from '../state/article.state';
import {tap} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'front';
  articles: Article[];
  articlesBJA: Article[];
  articleDetail: Article;
  singleArticle: Article;

  constructor(private articleService: ArticleService,
              private articleState: ArticleState) {
  }

  ngOnInit(): void {

    this.articleService.getAllArticles().subscribe(articles => {
      this.articles = articles, console.log(this.articles);
    });
    this.articleService.getArticleById(2).subscribe(article => {
      this.singleArticle = article, console.log(this.singleArticle);
    });

    this.setBaseData();
  }

  onDetail(id: number) {
    this.articleService.getArticleById(id).subscribe(article => {
      this.articleDetail = article, console.log(this.articleDetail);
    });
  }

  setBaseData() {

    this.articleState.articleGetAll().then(
      data => {
        this.articlesBJA = data;
      }
    );
  }
}
