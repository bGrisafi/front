import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Article} from '../models/article';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  private url = 'http://localhost:8000/api/articles';

  constructor(private http: HttpClient) {
  }

  getAllArticles(): Observable<Article[]> {
    return this.http.get<Article[]>(this.url);
  }

  getArticleById(id: number): Observable<Article> {
    return this.http.get<Article>(this.url + '/' + id);
  }
}
